<?php
/**
 * Order Total Module
 *
 *
 * @package - Signature Required
 * @copyright Copyright 2007 Numinix Technology http://www.numinix.com
 * @copyright Copyright 2003-2007 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ot_signature.php 2 2008-05-13 01:39:19Z numinix $
 */

  define('MODULE_ORDER_TOTAL_RESIDENTIAL_TITLE', 'Residential Address');
  define('MODULE_ORDER_TOTAL_RESIDENTIAL_DESCRIPTION', 'Residential Address');
  define('MODULE_ORDER_TOTAL_RESIDENTIAL_TEXT_ENTER_CODE', '<p>Is the order shipping to a residential address?</p>');
//eof
